# Cloud-Platform-Docker

Cloud-Platform Docker 快速体验版

## 环境准备
```
确保已安装docker、docker compose
确保如下端口未被占用：3306、8848、80、8765、9999、6379、9777
```

## 启动命令
```
# 首次启动，会因为数据库未初始化完导致失败；退出后再重新启动即可  
docker-compose up # 前台执行
docker-compose up -d # 后台执行
```

## 访问地址
```
http://localhost:80

账户：
超管：super/super
体验：admin/admin
```
